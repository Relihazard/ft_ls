/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/02 18:33:42 by agrossma          #+#    #+#             */
/*   Updated: 2018/03/15 14:18:06 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void		ft_front_back_split(t_list *source, t_list **front_ref,
				t_list **back_ref)
{
	t_list	*slow;
	t_list	*fast;

	if (source == NULL || source->next == NULL)
	{
		*front_ref = source;
		*back_ref = NULL;
	}
	else
	{
		slow = source;
		fast = source->next;
		while (fast != NULL)
		{
			fast = fast->next;
			if (fast != NULL)
			{
				slow = slow->next;
				fast = fast->next;
			}
		}
		*front_ref = source;
		*back_ref = slow->next;
		slow->next = NULL;
	}
}

static t_list	*ft_msort(t_list *a, t_list *b)
{
	t_list	*result;

	result = NULL;
	if (a == NULL)
		return (b);
	else if (b == NULL)
		return (a);
	if (ft_strcmp((char *)a->content, (char *)b->content) <= 0)
	{
		result = a;
		result->next = ft_msort(a->next, b);
	}
	else
	{
		result = b;
		result->next = ft_msort(a, b->next);
	}
	return (result);
}

static void		ft_msort_lst(t_list **lst)
{
	t_list	*head;
	t_list	*a;
	t_list	*b;

	head = *lst;
	if (head == NULL || head->next == NULL)
		return ;
	ft_front_back_split(head, &a, &b);
	ft_msort_lst(&a);
	ft_msort_lst(&b);
	*lst = ft_msort(a, b);
}

void			ft_print_errors(t_list *errors)
{
	char	*join1;
	char	*join2;
	char	*join3;
	size_t	len;
	size_t	err;

	ft_msort_lst(&errors);
	while (errors)
	{
		len = ft_strlen((char *)errors->content);
		err = errors->content_size - len;
		join1 = ft_strjoin("ft_ls: ", (char *)errors->content);
		join2 = ft_strjoin(join1, ": ");
		join3 = ft_strjoin(join2, strerror((int)err));
		ft_putendl_fd(join3, 2);
		errors = errors->next;
		ft_strdel(&join1);
		ft_strdel(&join2);
		ft_strdel(&join3);
	}
	ft_lstdel(&errors, &ft_free_link);
}

void			ft_print_error(char *access_path)
{
	char	*join1;
	char	*join2;
	char	*join3;

	join1 = ft_strjoin("ft_ls: ", access_path);
	join2 = ft_strjoin(join1, ": ");
	join3 = ft_strjoin(join2, strerror(errno));
	ft_putendl_fd(join3, 2);
	ft_strdel(&join1);
	ft_strdel(&join2);
	ft_strdel(&join3);
}
