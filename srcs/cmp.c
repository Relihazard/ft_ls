/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 15:51:44 by agrossma          #+#    #+#             */
/*   Updated: 2018/03/02 20:21:46 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_revmodcmp(t_fts *s1, t_fts *s2)
{
	if (s2->stat_p->st_mtime > s1->stat_p->st_mtime)
		return (-1);
	else if (s2->stat_p->st_mtime < s1->stat_p->st_mtime)
		return (1);
	else
		return (ft_revnamecmp(s1, s2));
}

int		ft_revnamecmp(t_fts *s1, t_fts *s2)
{
	return (ft_strcmp(s2->path, s1->path));
}

int		ft_modcmp(t_fts *s1, t_fts *s2)
{
	if (s2->stat_p->st_mtime > s1->stat_p->st_mtime)
		return (1);
	else if (s2->stat_p->st_mtime < s1->stat_p->st_mtime)
		return (-1);
	else
		return (ft_namecmp(s1, s2));
}

int		ft_namecmp(t_fts *s1, t_fts *s2)
{
	return (ft_strcmp(s1->path, s2->path));
}
