/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 15:31:54 by agrossma          #+#    #+#             */
/*   Updated: 2018/03/19 13:35:15 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		g_olongformat = 0;
int		g_orecursive = 0;
int		g_olistdot = 0;
int		g_oreversesort = 0;
int		g_otimesort = 0;
int		g_onosort = 0;
int		g_oslash = 0;
int		g_isdir = 0;
int		g_output = 0;
int		(*g_cmpfcn)(t_fts *, t_fts *) = NULL;
void	(*g_printfcn)(t_fts *) = NULL;

static void	ft_usage(void)
{
	ft_putendl_fd("usage: ft_ls [-"OPTIONS"] [file ...]", 2);
	exit(E_USAGE);
}

static void	ft_select_sort(void)
{
	if (g_oreversesort == 1)
	{
		if (g_otimesort == 1)
			g_cmpfcn = &ft_revmodcmp;
		else
			g_cmpfcn = &ft_revnamecmp;
	}
	else
	{
		if (g_otimesort == 1)
			g_cmpfcn = &ft_modcmp;
		else
			g_cmpfcn = &ft_namecmp;
	}
}

static void	ft_select_print(void)
{
	if (g_olongformat)
		g_printfcn = &ft_print_long;
	else
		g_printfcn = &ft_print_short;
}

static void	ft_fill_options(char opt)
{
	if (opt == 'R')
		g_orecursive = 1;
	else if (opt == 'a')
		g_olistdot = 1;
	else if (opt == 'l')
		g_olongformat = 1;
	else if (opt == 'r')
		g_oreversesort = 1;
	else if (opt == 't')
		g_otimesort = 1;
	else if (opt == 'f')
	{
		g_onosort = 1;
		g_olistdot = 1;
	}
	else if (opt == 'p')
		g_oslash = 1;
	else
		ft_usage();
}

int			main(int ac, char **av)
{
	int			opt;
	const char	*dotav[] = { ".", NULL };

	while ((opt = ft_getopt(ac, (const char **)av, OPTIONS)) != -1)
		ft_fill_options((char)opt);
	av += g_optind;
	ac -= g_optind;
	ft_select_sort();
	ft_select_print();
	ft_traverse(ac ? ac : 1, ac ? av : (char **)dotav);
	return (0);
}
