/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recursion.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/02 20:38:19 by agrossma          #+#    #+#             */
/*   Updated: 2018/03/19 14:55:10 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void	ft_inner_recurse_fts(char *access_path)
{
	t_fts		*files;
	DIR			*dirp;
	t_dirent	*dp;
	char		*path;

	files = NULL;
	(void)ft_printf("\n%s:\n", access_path);
	if ((dirp = opendir(access_path)))
	{
		while ((dp = readdir(dirp)))
		{
			path = ft_strjoin(access_path, "/");
			ft_fts_add(&files, ft_fts_new(path, dp->d_name, dp));
			ft_strdel(&path);
		}
		(void)closedir(dirp);
		if (files)
			ft_display(files);
	}
	else
		ft_print_error(access_path);
}

void		ft_recurse_fts(t_fts *lst)
{
	while (lst)
	{
		if (S_ISDIR(lst->stat_p->st_mode) && ft_strcmp(lst->name, ".")
			&& ft_strcmp(lst->name, "..")
			&& !(!g_olistdot && lst->name[0] == '.'))
			ft_inner_recurse_fts(lst->path);
		lst = lst->next;
	}
	ft_fts_del(&lst);
}
