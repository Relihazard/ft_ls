/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/28 11:44:45 by agrossma          #+#    #+#             */
/*   Updated: 2018/03/15 14:18:38 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		ft_free_link(void *content, size_t size)
{
	ft_memdel(&content);
	size = 0;
}

static void	ft_free_fts(t_fts **fts)
{
	ft_strdel(&(*fts)->access_path);
	ft_strdel(&(*fts)->path);
	(*fts)->path_len = 0;
	ft_strdel(&(*fts)->name);
	(*fts)->name_len = 0;
	ft_memdel((void **)&(*fts)->stat_p);
	(*fts)->next = NULL;
	ft_memdel((void **)fts);
}

void		ft_fts_del(t_fts **fts)
{
	t_fts	*tmp;

	tmp = NULL;
	while (*fts)
	{
		tmp = (*fts)->next;
		ft_free_fts(fts);
		*fts = tmp;
	}
}
