/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   traverse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 15:58:52 by agrossma          #+#    #+#             */
/*   Updated: 2018/03/15 16:07:11 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void			ft_display(t_fts *lst)
{
	!g_onosort ? ft_msort_fts(&lst) : NULL;
	g_printfcn(lst);
	g_orecursive ? ft_recurse_fts(lst) : NULL;
	ft_fts_del(&lst);
}

static void		ft_process_files(t_list *files)
{
	t_fts	*lst;

	lst = NULL;
	while (files)
	{
		ft_fts_add(&lst, ft_fts_new("", (char *)files->content, NULL));
		files = files->next;
	}
	ft_lstdel(&files, &ft_free_link);
	if (lst)
		ft_display(lst);
}

static void		ft_process_fts(t_fts *lst, int multidir)
{
	DIR				*dirp;
	t_dirent		*dp;
	static t_fts	*files = NULL;
	char			*path;

	while (lst)
	{
		path = lst->path[ft_strlen(lst->path) - 1] == '/'
			? ft_strdup(lst->path) : ft_strjoin(lst->path, "/");
		dirp = opendir(lst->path);
		while ((dp = readdir(dirp)))
			ft_fts_add(&files, ft_fts_new(path, dp->d_name, dp));
		ft_strdel(&path);
		(void)closedir(dirp);
		if (files)
		{
			g_output ? (void)ft_printf("\n") : NULL;
			multidir ? (void)ft_printf("%s:\n", lst->path) : NULL;
			g_output = 1;
			ft_display(files);
			files = NULL;
		}
		lst = lst->next;
	}
	ft_fts_del(&lst);
}

static void		ft_process_dirs(t_list *dirs, int multidir)
{
	t_fts	*lst;

	lst = NULL;
	while (dirs)
	{
		ft_fts_add(&lst, ft_fts_new("", (char *)dirs->content, NULL));
		dirs = dirs->next;
	}
	ft_lstdel(&dirs, &ft_free_link);
	!g_onosort ? ft_msort_fts(&lst) : NULL;
	ft_process_fts(lst, multidir);
	ft_fts_del(&lst);
}

void			ft_traverse(int ac, char **av)
{
	static t_list	*errors = NULL;
	static t_list	*dirs = NULL;
	static t_list	*files = NULL;
	DIR				*dirp;
	char			buf[1];

	while (ac--)
	{
		if (!(dirp = opendir(av[ac])))
			errno != ENOTDIR ?
			ft_lstadd(&errors, ft_lstnew(av[ac], ft_strlen(av[ac]) + errno)) :
			ft_lstadd(&files, ft_lstnew(av[ac], ft_strlen(av[ac]) + 1));
		else
		{
			g_olongformat && readlink(av[ac], buf, 1) != -1 ?
			ft_lstadd(&files, ft_lstnew(av[ac], ft_strlen(av[ac]) + 1)) :
			ft_lstadd(&dirs, ft_lstnew(av[ac], ft_strlen(av[ac]) + 1));
			(void)closedir(dirp);
		}
	}
	errors ? ft_print_errors(errors) : NULL;
	files ? ft_process_files(files) : NULL;
	files && dirs ? (void)ft_printf("\n") : NULL;
	g_isdir = 1;
	dirs ? ft_process_dirs(dirs, MULTIDIR(dirs->next || files)) : NULL;
}
