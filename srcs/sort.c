/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 16:58:33 by agrossma          #+#    #+#             */
/*   Updated: 2018/03/15 13:29:08 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void		ft_front_back_split(t_fts *source, t_fts **front_ref,
				t_fts **back_ref)
{
	t_fts	*slow;
	t_fts	*fast;

	if (source == NULL || source->next == NULL)
	{
		*front_ref = source;
		*back_ref = NULL;
	}
	else
	{
		slow = source;
		fast = source->next;
		while (fast != NULL)
		{
			fast = fast->next;
			if (fast != NULL)
			{
				slow = slow->next;
				fast = fast->next;
			}
		}
		*front_ref = source;
		*back_ref = slow->next;
		slow->next = NULL;
	}
}

static t_fts	*ft_msort(t_fts *a, t_fts *b)
{
	t_fts	*result;

	result = NULL;
	if (a == NULL)
		return (b);
	else if (b == NULL)
		return (a);
	if (g_cmpfcn(a, b) <= 0)
	{
		result = a;
		result->next = ft_msort(a->next, b);
	}
	else
	{
		result = b;
		result->next = ft_msort(a, b->next);
	}
	return (result);
}

void			ft_msort_fts(t_fts **lst)
{
	t_fts	*head;
	t_fts	*a;
	t_fts	*b;

	head = *lst;
	if (head == NULL || head->next == NULL)
		return ;
	ft_front_back_split(head, &a, &b);
	ft_msort_fts(&a);
	ft_msort_fts(&b);
	*lst = ft_msort(a, b);
}
