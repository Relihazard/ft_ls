/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 15:53:14 by agrossma          #+#    #+#             */
/*   Updated: 2018/03/19 15:03:16 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static char	*ft_get_color(t_stat *stat_p)
{
	if (S_ISDIR(stat_p->st_mode))
		return (JOIN(C_BOLD, C_CYAN));
	else if (S_ISLNK(stat_p->st_mode))
		return (C_MAGENTA);
	else if (S_ISSOCK(stat_p->st_mode))
		return (C_GREEN);
	else if (S_ISFIFO(stat_p->st_mode))
		return (C_BROWN);
	else if (S_ISREG(stat_p->st_mode) && (stat_p->st_mode & S_IXUSR))
		return (C_RED);
	else if (S_ISBLK(stat_p->st_mode))
		return (JOIN(C_BG_CYAN, C_BLUE));
	else if (S_ISCHR(stat_p->st_mode))
		return (JOIN(C_BG_BROWN, C_BLUE));
	else if (S_ISREG(stat_p->st_mode) && !(stat_p->st_mode & S_IXUSR))
		return (C_NONE);
	else
		return (C_NONE);
}

static char	*ft_get_date(t_fts *file)
{
	char	*str;
	char	*sub;
	char	*sub2;
	char	*buf;
	time_t	now;

	str = ctime(&file->stat_p->st_mtime);
	now = time(0);
	if ((now - 15778463) > file->stat_p->st_mtime
		|| now < file->stat_p->st_mtime)
	{
		buf = ft_strsub(str, 20, 4);
		sub = ft_strsub(str, 4, 6);
		sub2 = ft_strjoin(sub, "  ");
		str = ft_strjoin(sub2, buf);
		ft_strdel(&buf);
		ft_strdel(&sub);
		ft_strdel(&sub2);
	}
	else
		str = ft_strsub(str, 4, 12);
	return (str);
}

static void	ft_inner_print_long(t_fts *file, t_longf_size *sizes)
{
	char	*date;
	char	*rights;
	char	*uid;
	char	*gid;

	date = ft_get_date(file);
	rights = ft_get_rights(file->stat_p->st_mode);
	uid = UID_NAME(file->stat_p->st_uid);
	gid = GID_NAME(file->stat_p->st_gid);
	if (S_ISCHR(file->stat_p->st_mode) || S_ISBLK(file->stat_p->st_mode))
		(void)ft_printf("%s %*llu %-*s  %-*s %*d, %*d %s %s%s%s", rights,
			sizes->lnk_size, file->stat_p->st_nlink, sizes->owner_size, uid,
			sizes->group_size, gid, sizes->bytes_size - 4,
			major(file->stat_p->st_rdev), 3, minor(file->stat_p->st_rdev),
			date, ft_get_color(file->stat_p), file->name, C_RESET);
	else
		(void)ft_printf("%s %*llu %-*s  %-*s  %*llu %s %s%s%s", rights,
			sizes->lnk_size, file->stat_p->st_nlink, sizes->owner_size, uid,
			sizes->group_size, gid, sizes->bytes_size, file->stat_p->st_size,
			date, ft_get_color(file->stat_p), file->name, C_RESET);
	ft_strdel(&date);
	ft_strdel(&rights);
	!getpwuid(file->stat_p->st_uid) ? ft_strdel(&uid) : NULL;
	!getgrgid(file->stat_p->st_gid) ? ft_strdel(&gid) : NULL;
}

void		ft_print_long(t_fts *lst)
{
	char			*lnk_path;
	ssize_t			readlink_size;
	t_longf_size	*sizes;

	sizes = ft_get_sizes(lst);
	g_isdir ? (void)ft_printf("Total %d\n", sizes->blocks_size) : NULL;
	while (lst)
	{
		if (!(!g_olistdot && lst->name[0] == '.'))
		{
			ft_inner_print_long(lst, sizes);
			if (S_ISLNK(lst->stat_p->st_mode))
			{
				lnk_path = ft_strnew(4096);
				readlink_size = readlink(lst->path, lnk_path, 4096);
				lnk_path[readlink_size] = '\0';
				(void)ft_printf(" -> %s", lnk_path);
				ft_strdel(&lnk_path);
			}
			g_oslash && S_ISDIR(lst->stat_p->st_mode) ? ft_printf("/") : 0;
			(void)ft_printf("\n");
		}
		lst = lst->next;
	}
	ft_memdel((void **)&sizes);
}

void		ft_print_short(t_fts *lst)
{
	while (lst)
	{
		if (!(!g_olistdot && lst->name[0] == '.'))
		{
			(void)ft_printf("%s%s%s", ft_get_color(lst->stat_p),
				lst->name, C_RESET);
			g_oslash && S_ISDIR(lst->stat_p->st_mode) ? ft_printf("/") : 0;
			(void)ft_printf("\n");
		}
		lst = lst->next;
	}
}
