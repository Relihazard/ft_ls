/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fts.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 13:44:05 by agrossma          #+#    #+#             */
/*   Updated: 2018/03/15 16:14:24 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_fts	*ft_fts_new(char *path, char *name, t_dirent *dp)
{
	t_fts	*fts;

	if (!(fts = (t_fts *)ft_memalloc(sizeof(t_fts))))
		return (NULL);
	fts->access_path = ft_strdup(path);
	fts->path = ft_strjoin(fts->access_path, name);
	fts->path_len = ft_strlen(fts->path);
	fts->name = ft_strdup(name);
	fts->name_len = ft_strlen(fts->name);
	if (!(fts->stat_p = (t_stat *)ft_memalloc(sizeof(t_stat))))
		return (NULL);
	dp && dp->d_type != DT_LNK ?
		stat(fts->path, fts->stat_p) : lstat(fts->path, fts->stat_p);
	return (fts);
}

void	ft_fts_add(t_fts **fts, t_fts *new)
{
	t_fts	*tmp;

	tmp = NULL;
	if (*fts == NULL)
		*fts = new;
	else
	{
		tmp = *fts;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
}
