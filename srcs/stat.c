/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stat.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/03 15:09:07 by agrossma          #+#    #+#             */
/*   Updated: 2018/03/19 14:44:02 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static char	ft_get_uid(mode_t st_mode)
{
	if (st_mode & S_IXUSR & S_ISUID)
		return ('S');
	else if (st_mode & ~S_IXUSR & S_ISUID)
		return ('s');
	else if (st_mode & S_IXUSR)
		return ('x');
	else
		return ('-');
}

static char	ft_get_gid(mode_t st_mode)
{
	if (st_mode & S_IXGRP & S_ISGID)
		return ('S');
	else if (st_mode & ~S_IXGRP & S_ISGID)
		return ('s');
	else if (st_mode & S_IXGRP)
		return ('x');
	else
		return ('-');
}

static char	ft_get_sticky(mode_t st_mode)
{
	if (st_mode & S_ISVTX & S_IXOTH)
		return ('T');
	else if (st_mode & S_ISVTX & ~S_IXOTH)
		return ('t');
	else if (st_mode & S_IXOTH)
		return ('x');
	else
		return ('-');
}

char		*ft_get_rights(mode_t st_mode)
{
	char	*rights;

	rights = ft_strnew(10);
	(void)ft_printf("%c", S_ISBLK(st_mode) ? 'b' : '\0');
	(void)ft_printf("%c", S_ISCHR(st_mode) ? 'c' : '\0');
	(void)ft_printf("%c", S_ISDIR(st_mode) ? 'd' : '\0');
	(void)ft_printf("%c", S_ISLNK(st_mode) ? 'l' : '\0');
	(void)ft_printf("%c", S_ISSOCK(st_mode) ? 's' : '\0');
	(void)ft_printf("%c", S_ISFIFO(st_mode) ? 'p' : '\0');
	(void)ft_printf("%c", S_ISREG(st_mode) ? '-' : '\0');
	rights[0] = st_mode & S_IRUSR ? 'r' : '-';
	rights[1] = st_mode & S_IWUSR ? 'w' : '-';
	rights[2] = ft_get_uid(st_mode);
	rights[3] = st_mode & S_IRGRP ? 'r' : '-';
	rights[4] = st_mode & S_IWGRP ? 'w' : '-';
	rights[5] = ft_get_gid(st_mode);
	rights[6] = st_mode & S_IROTH ? 'r' : '-';
	rights[7] = st_mode & S_IWOTH ? 'w' : '-';
	rights[8] = ft_get_sticky(st_mode);
	rights[9] = ' ';
	return (rights);
}
