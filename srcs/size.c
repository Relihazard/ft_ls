/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   size.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/03 15:50:45 by agrossma          #+#    #+#             */
/*   Updated: 2018/03/19 14:20:25 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void		ft_get_easy_sizes(t_stat *st, t_longf_size *sizes)
{
	char		*max;
	static int	has_dev = 0;

	max = ft_itoa(st->st_nlink);
	sizes->lnk_size = FT_MAX(ft_strlen(max), sizes->lnk_size);
	ft_strdel(&max);
	max = ft_itoa(st->st_size);
	sizes->bytes_size = FT_MAX(ft_strlen(max), sizes->bytes_size);
	has_dev = (S_ISCHR(st->st_mode) || S_ISBLK(st->st_mode)) ? 1 : has_dev;
	sizes->bytes_size = has_dev
		? FT_MAX(sizes->bytes_size, 8) : sizes->bytes_size;
	ft_strdel(&max);
	sizes->blocks_size += st->st_blocks;
	if (getpwuid(st->st_uid))
		sizes->owner_size =
			FT_MAX(ft_strlen(getpwuid(st->st_uid)->pw_name), sizes->owner_size);
	else
		sizes->owner_size =
			FT_MAX(ft_strlen(ft_itoa(st->st_uid)), sizes->owner_size);
	if (getgrgid(st->st_gid))
		sizes->group_size =
			FT_MAX(ft_strlen(getgrgid(st->st_gid)->gr_name), sizes->group_size);
	else
		sizes->group_size =
			FT_MAX(ft_strlen(ft_itoa(st->st_gid)), sizes->group_size);
}

t_longf_size	*ft_get_sizes(t_fts *lst)
{
	t_fts			*files;
	t_longf_size	*sizes;

	files = lst;
	sizes = (t_longf_size *)ft_memalloc(sizeof(t_longf_size));
	while (files)
	{
		if (!(!g_olistdot && files->name[0] == '.'))
			ft_get_easy_sizes(files->stat_p, sizes);
		files = files->next;
	}
	return (sizes);
}
