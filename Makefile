# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/27 15:27:33 by agrossma          #+#    #+#              #
#    Updated: 2018/03/15 14:10:42 by agrossma         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SHELL		:= /bin/bash

#### Start of system configuration section ####

NAME		:= ft_ls
CC			:= gcc
CFLAGS		+= -Wall -Wextra -Werror
MKDIR		:= mkdir -p
RM			:= rm -f
RMDIR		:= rmdir
MAKE		:= make
ECHO		:= echo
QUIET		:= @

#### End of system configuration section ####

#### Start of files definition section ####

_INCLUDES	:= includes/
CFLAGS		+= -I$(_INCLUDES) -Ilibft/$(_INCLUDES)
_SRCS		:= srcs/
SRCS		:= \
	main.c \
	cmp.c \
	print.c \
	traverse.c \
	sort.c \
	free.c \
	fts.c \
	error.c \
	recursion.c \
	stat.c \
	size.c
_OBJS		:= objs/
OBJS		+= $(addprefix $(_OBJS), $(SRCS:.c=.o))

#### End of files definition section ####

#### Start of linking configuration section ####

LD			:= gcc
LDFLAGS		:=
LDLIBS		:= -Llibft/ -lft

#### End of linking configuration section ####

#### Start of rules definition section ####

.PHONY: all clean fclean re

all: $(NAME)

$(NAME): libft/libft.a $(_OBJS) $(OBJS)
	$(QUIET)$(ECHO) "LD	$@"
	$(QUIET)$(LD) $(OBJS) $(LDFLAGS) $(LDLIBS) -o $@

$(_OBJS):
	$(QUIET)$(MKDIR) $(_OBJS)

libft/libft.a:
	$(QUIET)$(MAKE) -C libft/

$(_OBJS)%.o: $(_SRCS)%.c
	$(QUIET)$(ECHO) "CC	$(notdir $@)"
	$(QUIET)$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(QUIET)$(MAKE) -C libft/ $@
	$(QUIET)$(ECHO) "RM	$(_OBJS)"
	$(QUIET)$(RM) $(OBJS)
	$(QUIET)if [ -d "$(_OBJS)" ]; then \
		$(RMDIR) $(_OBJS); \
	fi

fclean: clean
	$(QUIET)$(MAKE) -C libft/ $@
	$(QUIET)$(ECHO) "RM	$(NAME)"
	$(QUIET)$(RM) $(NAME)

re: fclean all

#### End of rules definition section ####

