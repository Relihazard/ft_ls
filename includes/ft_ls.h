/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 13:48:05 by agrossma          #+#    #+#             */
/*   Updated: 2018/03/19 14:44:06 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

/*
** Includes for authorized functions
** libft.h for the libft functions
** unistd.h for write(2), readlink(2)
** dirent.h for opendir(3), readdir(3) and closedir(3)
** sys/stat.h for stat(2) and lstat(2)
** sys/types.h and pwd.h for getpwuid(3)
** grp.h for getgrgid(3)
** uuid/uuid.h for getpwuid(3) and getgrgid(3)
** sys/xattr for listxattr(3) and getxattr(2)
** time.h for time(3) and ctime(3)
** stdlib.h for malloc(3), free(3) and exit(3)
** stdio.h for perror(3)
** string.h for strerror(3)
*/
# include <libft.h>
# include <dirent.h>
# include <grp.h>
# include <pwd.h>
# include <stdio.h>
# include <sys/errno.h>
# include <stdlib.h>
# include <string.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <sys/xattr.h>
# include <time.h>
# include <unistd.h>
# include <uuid/uuid.h>
# include <sys/ioctl.h>

# define E_USAGE 1

# define OPTIONS "Raflprt"

# define UID_NAME(x) (getpwuid(x) ? getpwuid(x)->pw_name : ft_itoa(x))
# define GID_NAME(x) (getgrgid(x) ? getgrgid(x)->gr_name : ft_itoa(x))

# define MULTIDIR(x) (x ? 1 : 0)

# define JOIN(x, y) (x""y)

# define C_NONE "\x1b[0m"
# define C_RESET "\x1b[0m"

# define C_BOLD "\x1b[1m"
# define C_BLUE "\x1b[34m"
# define C_CYAN "\x1b[36m"
# define C_BROWN "\x1b[33m"
# define C_MAGENTA "\x1b[35m"
# define C_RED "\x1b[31m"
# define C_GREEN "\x1b[32m"

# define C_BG_BROWN "\x1b[43m"
# define C_BG_CYAN "\x1b[46m"

typedef struct stat		t_stat;
typedef struct dirent	t_dirent;
typedef struct winsize	t_winsize;

typedef struct			s_fts
{
	char			*access_path;
	char			*path;
	size_t			path_len;
	char			*name;
	size_t			name_len;
	t_stat			*stat_p;
	struct s_fts	*next;
}						t_fts;

typedef struct			s_longf_size
{
	size_t	lnk_size;
	size_t	owner_size;
	size_t	group_size;
	size_t	bytes_size;
	size_t	major_size;
	size_t	minor_size;
	size_t	blocks_size;
}						t_longf_size;

extern int				g_olongformat;
extern int				g_orecursive;
extern int				g_olistdot;
extern int				g_oreversesort;
extern int				g_otimesort;
extern int				g_onosort;
extern int				g_oslash;
extern int				g_isdir;
extern int				g_output;
extern int				(*g_cmpfcn)(t_fts *, t_fts *);
extern void				(*g_printfcn)(t_fts *);

int						ft_revmodcmp(t_fts *s1, t_fts *s2);
int						ft_revnamecmp(t_fts *s1, t_fts *s2);
int						ft_modcmp(t_fts *s1, t_fts *s2);
int						ft_namecmp(t_fts *s1, t_fts *s2);

t_fts					*ft_fts_new(char *path, char *name, t_dirent *dp);
void					ft_fts_add(t_fts **fts, t_fts *new);

void					ft_msort_fts(t_fts **lst);

void					ft_print_error(char *access_path);
void					ft_print_errors(t_list *errors);

t_longf_size			*ft_get_sizes(t_fts *lst);

char					*ft_get_rights(mode_t st_mode);

void					ft_print_long(t_fts	*lst);
void					ft_print_short(t_fts *lst);

void					ft_recurse_fts(t_fts *lst);

void					ft_display(t_fts *lst);
void					ft_traverse(int ac, char **av);

void					ft_free_link(void *content, size_t size);
void					ft_fts_del(t_fts **fts);

#endif
